using System;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using JetBrains.Annotations;

namespace Gyro.Daq
{
    /// <summary>
    /// The class helps to form and parse LGC protocol messages
    /// If full message is ':CMS M0 O0 L0 D167;'
    /// Where:
    ///   AddressAndData = "CMS M0 O0 L0 "
    ///   Data = "S M0 O0 L0 "
    ///   To = 'C'
    ///   From = 'M'
    ///   Envelope is ":(...)D167;"
    ///          ':' - start sequence
    ///          ';' - end sequence
    ///          'D167' - XXXX Crc16 two bytes
    /// </summary>
    /// <example>
    /// // Create message from parts. To Master with GyroCompass status string
    /// var message = new Message('M', 'G', $"S M{mode} L{lifeTime} P{parked} TR{remain} E{error} ");
    /// message.ToEnvelope();      // ToString will do the same
    /// // :GMS ML L1064 P0 TR156 E0 9671;
    /// 
    /// //Parse existing message
    /// string request = :CMS M0 O0 L0 D167;
    /// Message message;
    /// try
    /// {
    ///    message = Message.FromEnvelope(request);
    /// }
    /// catch (Exception e)
    /// {
    ///    if (e is ArgumentException || e is FormatException)
    ///    {
    ///        // Cant convert the string to message
    ///    }
    ///    if (e is ChecksummException)
    ///    {
    ///        // ...
    ///    }
    ///}
    /// </example>
    public class Message
    {
        public const string EnvelopeLengthErrorMessage =
            "Envelope length is too short. It should be at least 7 symbols without ':' and ';' or atleast 9 symbols with ':' and ';'";

        /// <summary> To address char </summary>
        public char To => AddressAndData[0];

        /// <summary> From address char </summary>
        public char From => AddressAndData[1];

        /// <summary> Data  string with addresses chars. Like MCS </summary>
        public string AddressAndData { get; }

        /// <summary> Message data </summary>
        public string Data => AddressAndData.Substring(2);

        public const char BeginChar = ':';
        public const char EndChar = ';';

        public UInt16 Crc16
        {
            get
            {
                var bytes = Encoding.ASCII.GetBytes(AddressAndData);
                var ret = GetCrc16(bytes, (ushort) bytes.Length);
                return ret;
            }
        }

        /// <summary> Takes string that contains Address of  </summary>
        /// <param name="addressAndData"></param>
        public Message(string addressAndData)
        {
            AddressAndData = addressAndData;
        }

        public Message(char to, string data)
        {
            AddressAndData = new string(new[] {to, 'C'}) + data;
        }

        public Message(char to, char from, string data)
        {
            AddressAndData = new string(new[] {to, from}) + data;
        }

        /// <summary>Parses an envelope and creates a message from it</summary>
        /// <param name="envelope"></param>
        /// <exception cref="ArgumentException">envelope is too short or string is null </exception>
        /// <exception cref="ChecksummException">Wrong checksumm</exception>
        /// <exception cref="FormatException">Unab</exception>
        /// <returns></returns>
        [NotNull]
        public static Message FromEnvelope(string envelope)
        {
            envelope = envelope.Trim();
            envelope = envelope.Trim(BeginChar, EndChar);

            if (envelope.Length < 7) throw new ArgumentException(EnvelopeLengthErrorMessage, nameof(envelope));

            // Сompare CRC

            // Get parsed crc
            int parsedCrc;
            try
            {
                parsedCrc = Int32.Parse(envelope.Substring(envelope.Length - 4), System.Globalization.NumberStyles.HexNumber);
            }
            catch (Exception ex)
            {
                throw new FormatException("Can't parse CRC of the message", ex);
            }
            
            string addressAndData = envelope.Substring(0, envelope.Length - 4); //1outMessage.Length - 4 because crc + ':' = 5
            var bytes = Encoding.ASCII.GetBytes(addressAndData);
            int calcCrc = GetCrc16(bytes, (ushort)bytes.Length);
            if (parsedCrc != calcCrc)
            {
                throw new ChecksummException(calcCrc, parsedCrc, bytes);
            }
            return new Message(addressAndData);
        }

        public string ToEnvelope()
        {
            return $":{AddressAndData}{Crc16:X4};";
        }

        /// <summary>
        /// Name  : CRC-16 CCITT-FALSE
        /// Poly  : 0x1021    x^16 + x^12 + x^5 + 1
        /// Init  : 0xFFFF
        /// Revert: false
        /// XorOut: 0x0000
        /// Check : 0x29B1 ("123456789")
        ///         *  byte[] oo = new byte[9] { 0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39};
        /// MaxLen: 4095 байт (32767 бит) - обнаружение одинарных, двойных, тройных и всех нечетных ошибок
        /// </summary>
        /// <param name="pcBlock">An array for which CRC16 is to be calculated</param>
        /// <param name="len">Number of bytes to be processed</param>
        /// <param name="startIndex">Index of the array to start with</param>
        /// <returns>UInt16 CRC16 value</returns>
        [SuppressMessage("ReSharper", "RedundantCast")]
        public static ushort GetCrc16(byte[] pcBlock, int len, int startIndex=0)
        {
            ushort crc = 0xFFFF;
            for (int iter = 0; iter < len; iter++)
            {
                crc ^= (ushort)((ushort)pcBlock[iter] << (ushort)8);

                for (int i = 0; i < 8; i++)
                    crc = (((ushort)crc & (ushort)0x8000) != 0) ? (ushort)(((ushort)crc << (ushort)1) ^ (ushort)0x1021) : (ushort)((ushort)crc << (ushort)1);
            }
            return crc;
        }


        /// <summary> Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>. </summary>
        /// <returns> A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>. </returns>
        public override string ToString()
        {
            return ToEnvelope();
        }
    }
}
