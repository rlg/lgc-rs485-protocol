//------------------------------------------------------------------------------
/// Optimized CRC-16/CCITT-FALSE calculation
/// 
/// Polynomial: x^16 + x^12 + x^5 + 1 (0x1021)
/// Initial value: 0xFFFF
//------------------------------------------------------------------------------
static uint16_t msl_update_crc16 (uint16_t crc, uint8_t data)
{
    uint16_t i;

    crc = crc ^ ((uint16_t)data << 8);
    for (i=0; i<8; i++)
    {
        if (crc & 0x8000)
            crc = (uint16_t) ((crc << 1) ^ 0x1021);
        else
            crc <<= 1;
    }

    return crc;
}
    

//------------------------------------------------------------------------------
/// Calculates crc 16 for string like char* buffer. 
/// Calculation is stopped when '\0' occured or checkLen is reached
/// @see UpdateCrc16 for details about crc
//------------------------------------------------------------------------------
static uint16_t msl_calculate_crc16(char* data, uint_fast16_t checkLen)
{
    uint_fast16_t i = 0;
    uint16_t crc = 0xFFFF;
    for(; i<checkLen && data[i]!=0; i++)
    {   
        crc = msl_update_crc16(crc, (uint8_t)data[i]);
    }
    return crc;
}

