public class CyclicalRedundancy
{
        /// <summary>
        /// Name  : CRC-16/CCITT-FALSE
        /// Poly  : 0x1021    x^16 + x^12 + x^5 + 1
        /// Init  : 0xFFFF
        /// Revert: false
        /// XorOut: 0x0000
        /// Check : 0x29B1 ("123456789")
        ///         *  byte[] oo = new byte[9] { 0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39};
        /// MaxLen: 4095 байт (32767 бит) - обнаружение одинарных, двойных, тройных и всех нечетных ошибок
        /// </summary>
        /// <param name="pcBlock">An array for which CRC16 is to be calculated</param>
        /// <param name="len">Number of bytes to be processed</param>
        /// <param name="startIndex">Index of the array to start with</param>
        /// <returns>UInt16 CRC16 value</returns>
        [SuppressMessage("ReSharper", "RedundantCast")]
        public static ushort GetCrc16(byte[] pcBlock, int len, int startIndex=0)
        {
            ushort crc = 0xFFFF;
            for (int iter = 0; iter < len; iter++)
            {
                crc ^= (ushort)((ushort)pcBlock[iter] << (ushort)8);

                for (int i = 0; i < 8; i++)
                    crc = (((ushort)crc & (ushort)0x8000) != 0) ? (ushort)(((ushort)crc << (ushort)1) ^ (ushort)0x1021) : (ushort)((ushort)crc << (ushort)1);
            }
            return crc;
        }
}